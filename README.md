# Description #
This is a homework of OOP class HTLin. The aim is to build a pet fighting game using Java, with some classes to be extended.

The original homework description is [here](https://www.csie.ntu.edu.tw/~htlin/course/oop13spring/hw4/hw4.php).