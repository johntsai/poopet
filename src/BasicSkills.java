package ntu.csie.oop13spring;
import java.lang.*;
import java.util.*;

// Basic Actions
class Attack extends POOMySkill{
	static int dmg;
	// constructor
	public Attack(int range){
		super("Attack","Attack enemy in range.(cost: 3 AGI)",range,1,false,(byte)1); 
		dmg = 250;
	}
	public Attack(String a1,String a2,int range,boolean straight){
		super(a1,a2,range,1,straight,(byte)1);
	}
	public Attack(String a1,String a2,int range,boolean straight,byte type){
		super(a1,a2,range,1,straight,type );
	}
	public void setVar(POOMyArena arena){
		this.dmg = arena.getNextPet().getATK();
	}
	public void act(POOPet pet){
		act(pet,dmg);
	}
	public void act(POOPet pet,int dmg){
		POOMyPet myP = (POOMyPet) pet;
		myP.setHP( Math.max(myP.getHP() - Math.max(dmg-myP.getDEF(),0),0) );
		System.out.printf("%c[1;31;40mDealt %d damage to %s(%s%d)!\n%c[m",27,Math.max(dmg-myP.getDEF(),0),myP.getName(),myP.getShortName(),myP.getID(),27);
	}
	public boolean check(POOMyPet self){
		return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI() - 3);
	}
}
class Defend extends POOMySkill{
	// constructor
	public Defend(){
		super("Defend","Increase your DEF for 125 in 5 turns.(cost: 5 AGI)",0,0,false,(byte)0);
	}
	public void act(POOPet pet){
		POOMyPet myP = (POOMyPet) pet;
		myP.setDEF( myP.getDEF()+125 );
		myP.setStatus("Defend");
	}
	public boolean check(POOMyPet self){
		return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI() - 5);
	}
	public void setVar(POOMyArena arena){};
}
class Rest extends POOMySkill{
	// constructor
	public Rest(){
		super("Rest","Recover your HP and MP.(cost: 3 AGI)",0,0,false,(byte)0);
	}
	public void act(POOPet pet){
		POOMyPet myP = (POOMyPet) pet;
		myP.setHP( Math.min(myP.getHP() + myP.getSTA(),1000) );
		myP.setMP( Math.min(myP.getMP() + 200,1000 ));
	}
	public boolean check(POOMyPet self){
		return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI() - 3);
	}
	public void setVar(POOMyArena arena){};
}
