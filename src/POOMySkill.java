package ntu.csie.oop13spring;
import java.lang.*;

public abstract class POOMySkill extends POOSkill{
	// data
	private String name;
	private String descript;
	private int range;
	private int target;	// 0 - self only, 1 - enemy
	private boolean straight;	
	private byte type;
	// constructor
	public POOMySkill (String a1, String a2, int range, int target,boolean straight,byte type){
		name=a1;
		descript=a2; 
		this.range = range;
		this.target = target;
		this.straight = straight;
		this.type = type;
	}
	// method
	public byte getType(){
		return type;
	}
	public boolean isStraight(){
		return straight;
	}
	public String getName(){
		return name;
	}
	public String getDes(){
		return descript;
	}
	public int getRange(){
		return range;
	}
	public int getTarget(){
		return target;
	}
    /**
       the skill is defined as "something"
       that can change the HP or AGI of any POOPet
    */
    public abstract void act(POOPet pet);
	/** Check if the skill is able to act(have enough AGI, MP, HP...) **/
	public abstract boolean check(POOMyPet self);
	/** Set variable by the caster.**/
	public abstract void setVar(POOMyArena arena);
	/** And this will cost MP/HP/AGI on caster itself.**/
	public abstract void cost(POOMyPet self);
}
