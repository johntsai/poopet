package ntu.csie.oop13spring;
import java.util.*;
import java.lang.*; 

class shieldSmash extends Attack{
	static POOPosition center;
	static byte[][] obs = new byte[20][20];
	static boolean dirRight,dirLeft,dirUp,dirDown; 
	// constructor
	public shieldSmash(){
		super("Shield Smash","(Def->SpeM)\n\tDeal 400 damage and knock back the enemy.(Cost 250 MP, 5 AGI)",1,false);
	}
	public shieldSmash(String a1,String a2, int range,boolean straight){
		super(a1,a2,range,straight);
	}
	public shieldSmash(String a1,String a2, int range,boolean straight,byte type){
		super(a1,a2,range,straight,type);
	}
	public void setDir(POOPet pet){
		POOMyPet myP = (POOMyPet) pet;
		if(center.x < myP.getPos().x){
			dirRight = true;
			dirLeft = false;
		}else if(center.x > myP.getPos().x){
			dirRight = false;
			dirLeft = true;
		}else{
			dirRight = false;
			dirLeft = false;
		}
		if(center.y > myP.getPos().y){
			dirUp = true;
			dirDown = false;
		}else if(center.y < myP.getPos().y){
			dirUp = false;
			dirDown = true; 
		}else{
			dirUp = false;
			dirDown = false;
		}
	}
	public void knockBack(POOPet pet, int dist){
		setDir(pet);
		POOMyPet myP = (POOMyPet) pet;
		// if not straight -> sqrt
		if((dirRight||dirLeft)&&(dirUp||dirDown))
			dist = (int) Math.floor(Math.sqrt(dist));
		// knock back!
		int x2,y2;
		for(int i=0;i<dist;i++){
			x2=myP.getPos().x;
			y2=myP.getPos().y;
			if(dirRight && x2<19)
				x2 = x2+1;
			else if(dirLeft && x2>0)
				x2 = x2-1;
			if(dirUp && y2>0)   
				y2 = y2-1;
			else if(dirDown && y2<19)
				y2 = y2+1;
			if(obs[y2][x2]==0)
				myP.setPos(x2,y2);
		}
	}
	public void act(POOPet pet){
		act(pet,400);
		knockBack(pet,2);
		// Stun for 1 turn
		pet.setAGI(Math.max(pet.getAGI()-1,0));
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<3 || self.getMP()<250)
			return false;
		else
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-5);
		self.setMP(self.getMP()-250);
	}
	public void setVar(POOMyArena arena){
		center = arena.getNextPet().getPos();
		obs = arena.getObs();
	}
}
class ironDefense extends POOMySkill{
	private String status = "Iron Defense";
	public ironDefense(){
		super("Iron Defense","(Def->Def)\n\tBecome invincible in 3 turn. (Cost 250 MP, 10 AGI)",0,0,false,(byte)0);
	}
	public ironDefense(String a1,String a2,String status){
		super(a1,a2,0,0,false,(byte)0);
		this.status = status;
	}
	public ironDefense(String a1,String a2){
		super(a1,a2,0,0,false,(byte)0);
	}
	public void act(POOPet pet){
		POOMyPet myP = (POOMyPet) pet;
		myP.setStatus(status);
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<10 || self.getMP()<250)
			return false;
		else 
			return true;
	}
	public void setVar(POOMyArena arena){};
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-3);
		self.setMP(self.getMP()-250);
	}
}
class rage extends POOMySkill{ 
	public rage(){
		super("Rage","(Atk->Atk->SpeM)\n\tATK*1.5. Zero your DEF. (Cost 400 MP)",0,0,false,(byte)0);
	}
	public void act(POOPet pet){
		POOMyPet myP = (POOMyPet) pet;
		myP.setATK((int)(myP.getATK()*1.5));
		myP.setDEF(0);
	}
	public boolean check(POOMyPet self){
		if(self.getMP()<400)
			return false;
		else 
			return true;
	}
	public void setVar(POOMyArena arena){};
	public void cost(POOMyPet self){
		self.setMP(self.getMP()-400);
	}
}
public class POOWarrior extends POOMyPet{
	// Data
	/*
	private ArrayList<POOMySkill> allSkill = new ArrayList<POOMySkill>(0);
	protected ArrayList<String> skillOrder = new ArrayList<String>(0);*/ 
	static protected ArrayList<POOMySkill> warriorSkill = new ArrayList<POOMySkill>(0);
	// construct Skill List of Warrior
	static{
		// three basic
		warriorSkill.add(new Attack(1));
		warriorSkill.add(new Defend());
		warriorSkill.add(new Rest());
		warriorSkill.add(new shieldSmash());
		warriorSkill.add(new ironDefense());
		warriorSkill.add(new rage());
	}
	// Constructor
	public POOWarrior(){
		super("Warrior",'W',warriorSkill);
		setDEF(75);
		setSTA(75);
		setAGImax(15);
		setSPE(5);
		setPos();		
	}
	// Complete the abstract
	protected POOMySkill recogSkill(){
		int SOSize = skillOrder.size();
		// Rage
		if(SOSize>=3){
			if(skillOrder.get(SOSize-1).equals("SpeM") && skillOrder.get(SOSize-2).equals("Atk") && skillOrder.get(SOSize-3).equals("Atk"))
				return warriorSkill.get(5);
		}
		// Iron Defense
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Def") && skillOrder.get(SOSize-2).equals("Def"))
				return warriorSkill.get(4);
		}
		// Shield Smash
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("SpeM") && skillOrder.get(SOSize-2).equals("Def"))
				return warriorSkill.get(3);
		}
		// Basic Ones
		if(skillOrder.get(SOSize-1).equals("Atk"))
			return warriorSkill.get(0);
		else if(skillOrder.get(SOSize-1).equals("Def"))
			return warriorSkill.get(1);
		else if(skillOrder.get(SOSize-1).equals("Rest"))
			return warriorSkill.get(2);
		else 
			return null;
	}
}