package ntu.csie.oop13spring;
import java.util.*;
import java.lang.*; 

class ambush extends ironDefense{
	// constructor
	public ambush(){
		super("Ambush","(SpeM) Hide yourself and become invincible.(Cost 100 MP, 3 AGI)","Invincible");
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<3 || self.getMP()<100)
			return false;
		else 
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-3);
		self.setMP(self.getMP()-100);
	}
}
class assassinate extends Attack{
	static POOPosition target;
	static byte[][] obs = new byte[20][20];
	// constructor
	public assassinate(){
		super("Assassinate","(SpeM->Atk)\n\tAttack any enemy and move near to him.(Cost 100 MP, 5 AGI)",99,false);
	}
	public assassinate(String a1, String a2,int range, boolean straight){
		super(a1,a2,range,straight);
	}
	public void setVar(POOMyArena arena){
		obs = arena.getObs();
		dmg = arena.getNextPet().getATK();
	}
	public void act(POOPet pet){
		act(pet,dmg);
		POOMyPet myP = (POOMyPet) pet;
		target = myP.getPos();
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<5 || self.getMP()<100)
			return false;
		else 
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-5);
		self.setMP(self.getMP()-100);
		randMove(self);
	}
	void randMove(POOMyPet self){
		Random ran = new Random();
		int x,y,x2,y2;
		do{
			do{
				y = ran.nextInt(3)-1;
				x = ran.nextInt(3)-1;
				y2 = target.y+y;
				x2 = target.x+x;
			}while(x==0 && y==0);
		}while(x2<0 || x2>19 || y2<0 || y2>19 || obs[y2][x2]!=0);
		self.setPos(x2,y2);
	}
}
class sonicSlash extends assassinate{
	// constructor
	public sonicSlash(){
		super("Sonic Slash","(Mov->Atk)\n\tCross and attack the enemy extremely fast.(Cost 200 MP,1 AGI)",1,false);
	}
	public sonicSlash(String a1,String a2,int range, boolean straight){
		super(a1,a2,range,straight);
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<1 || self.getMP()<200)
			return false;
		else 
			return true;
	}
	public void cross(POOMyPet self){
		POOPosition myPos = self.getPos();
		int x2 = myPos.x + 2*(target.x - myPos.x);
		int y2 = myPos.y + 2*(target.y - myPos.y);
		if(x2<0 || x2>19 || y2<0 || y2>19 || obs[y2][x2]!=0){	// invalid place
			return;
		}else{
			self.setPos(x2,y2);
		}
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-1);
		self.setMP(self.getMP()-200);
		cross(self);
	}
}
class doubleAttack extends sonicSlash{
	// constructor
	public doubleAttack(){
		super("Double Attack","(Atk->Atk)\n\tCross the target and deal double damage(Cost 200 MP, 6 AGI)",2,false);
	}
	public void act(POOPet pet){ 
		act(pet,dmg*2);
		POOMyPet myP = (POOMyPet) pet;
		target = myP.getPos();
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<6 || self.getMP()<200)
			return false;
		else 
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-6);
		self.setMP(self.getMP()-200);
		cross(self);
	}
}
public class POOAssassin extends POOMyPet{
	// Data
	static protected ArrayList<POOMySkill> assassinSkill = new ArrayList<POOMySkill>(0);
	// construct Skill List of Archer
	static{
		// three basic
		assassinSkill.add(new Attack(2));
		assassinSkill.add(new Defend());
		assassinSkill.add(new Rest());
		assassinSkill.add(new ambush());
		assassinSkill.add(new assassinate());
		assassinSkill.add(new sonicSlash());
		assassinSkill.add(new doubleAttack());
	}
	// Constructor
	public POOAssassin(){
		super("Assassin",'S',assassinSkill);
		setDEF(25);
		setSTA(75);
		setAGImax(10);
		setSPE(10);
		setPos();		
	}
	// Complete the abstract
	protected POOMySkill recogSkill(){
		int SOSize = skillOrder.size();
		// double Attack
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Atk") && skillOrder.get(SOSize-2).equals("Atk"))
				return assassinSkill.get(6);
		}
		// Sonic Slash
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Atk") && skillOrder.get(SOSize-2).equals("Mov"))
				return assassinSkill.get(5);
		}
		// Assassinate
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Atk") && skillOrder.get(SOSize-2).equals("SpeM"))
				return assassinSkill.get(4);
		}
		// Ambush
		if(SOSize>=1){
			if(skillOrder.get(SOSize-1).equals("SpeM"))
				return assassinSkill.get(3);
		}
		// Basic Ones
		if(skillOrder.get(SOSize-1).equals("Atk"))
			return assassinSkill.get(0);
		else if(skillOrder.get(SOSize-1).equals("Def"))
			return assassinSkill.get(1);
		else if(skillOrder.get(SOSize-1).equals("Rest"))
			return assassinSkill.get(2);
		else 
			return null;
	}
}

