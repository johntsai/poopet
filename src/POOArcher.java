package ntu.csie.oop13spring;
import java.util.*;
import java.lang.*; 

class headShot extends Attack{
	// constructor
	public headShot(){
		super("Head Shot","(SpeM->Atk)\n\tDeal 500 dmg to an enemy in range.(Cost 200 MP, 7 AGI)",10,false);
	}
	public void setVar(POOMyArena arena){
		dmg=500;
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<7 || self.getMP()<200)
			return false;
		else
			return true;
	} 
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-7);
		self.setMP(self.getMP()-200);
	}
}
class aim extends ironDefense{
	// constructor
	public aim(){
		super("Aim","(SpeM) Prepare for a strong skill.(Cost 100 MP, 3 AGI)","Aiming");
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<3 || self.getMP()<100)
			return false;
		else 
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-3);
		self.setMP(self.getMP()-100);
	}
}
class bowSmash extends shieldSmash{
	// constructor
	public bowSmash(){
		super("Bow Smash","(Mov->Atk)\n\tHit with bow and knock back the target in short range.(Cost 150MP, 4 AGI)",2,false);
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<4 || self.getMP()<150)
			return false;
		else 
			return true;
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-4);
		self.setMP(self.getMP()-150);
	}
	public void act(POOPet pet){
		act(pet,dmg);
		knockBack(pet,2);
		// Stun for 1 turn
		pet.setAGI(Math.max(pet.getAGI()-1,0));
	}
}
class pierceShot extends shieldSmash{
	static POOPet[] allPets = new POOPet[4];
	static POOPosition target;
	// constructor
	public pierceShot(){
		super("Pierce Shot","(SpeM->SpeM)\n\tAttack straightly and pierce 3 steps.(Cost 150 MP, 7 AGI)",20,true,(byte)1);
	}
	public void setVar(POOMyArena arena){
		allPets = arena.getAllPets();
		center = arena.getNextPet().getPos();
	}
	public boolean check(POOMyPet self){
		if(self.getAGI()<7 || self.getMP()<150)
			return false;
		else 
			return true;
	}
	public boolean inPierceRange(POOPosition pos,int dist){
		// if not straight -> dist/2
		if((dirRight||dirLeft)&&(dirUp||dirDown))
			dist = (int) Math.floor(dist/2);
		int x2=target.x;
		int y2=target.y;
		for(int i=0;i<dist;i++){
			if(dirRight && x2<19)
				x2 = x2+1;
			else if(dirLeft && x2>0)
				x2 = x2-1;
			if(dirUp && y2>0)   
				y2 = y2-1;
			else if(dirDown && y2<19)
				y2 = y2+1;
			if(y2==pos.y && x2==pos.x)
				return true;
		}
		return false;
	}
	public void act(POOPet pet){
		act(pet,dmg);
		target =((POOMyPet)pet).getPos();
		setDir(pet);
		// pierce
		for(int i=0;i<allPets.length;i++){	
			POOMyPet myP = (POOMyPet) allPets[i];
			if(inPierceRange(myP.getPos(),3))
				act(allPets[i],dmg);
		}
	}
	public void cost(POOMyPet self){
		self.setAGI(self.getAGI()-7);
		self.setMP(self.getMP()-150);
	}
}
public class POOArcher extends POOMyPet{
	// Data
	static protected ArrayList<POOMySkill> archerSkill = new ArrayList<POOMySkill>(0);
	// construct Skill List of Archer
	static{
		// three basic
		archerSkill.add(new Attack(10));
		archerSkill.add(new Defend());
		archerSkill.add(new Rest());
		archerSkill.add(new aim());
		archerSkill.add(new headShot());
		archerSkill.add(new bowSmash());
		archerSkill.add(new pierceShot());
	}
	// Constructor
	public POOArcher(){
		super("Archer",'A',archerSkill);
		setDEF(0);
		setSTA(125);
		setAGImax(12);
		setSPE(7);
		setPos();		
	}
	// Complete the abstract
	protected POOMySkill recogSkill(){
		int SOSize = skillOrder.size();
		// Pierce Shot
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("SpeM") && skillOrder.get(SOSize-2).equals("SpeM"))
				return archerSkill.get(6);
		}
		// Head Shot
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Atk") && skillOrder.get(SOSize-2).equals("SpeM"))
				return archerSkill.get(4);
		}
		// Aim
		if(SOSize>=1){
			if(skillOrder.get(SOSize-1).equals("SpeM"))
				return archerSkill.get(3);
		}
		// Bow Smash
		if(SOSize>=2){
			if(skillOrder.get(SOSize-1).equals("Atk") && skillOrder.get(SOSize-2).equals("Mov"))
				return archerSkill.get(5);
		}
		// Basic Ones
		if(skillOrder.get(SOSize-1).equals("Atk"))
			return archerSkill.get(0);
		else if(skillOrder.get(SOSize-1).equals("Def"))
			return archerSkill.get(1);
		else if(skillOrder.get(SOSize-1).equals("Rest"))
			return archerSkill.get(2);
		else 
			return null;
	}
}
