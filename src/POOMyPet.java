package ntu.csie.oop13spring;
import java.util.*;
import java.lang.*;

public abstract class POOMyPet extends POOPet{
	// DATA
	private POOPosition pos;
	private ArrayList<POOMySkill> allSkill = new ArrayList<POOMySkill>(0);
	protected ArrayList<String> skillOrder = new ArrayList<String>(0);
	private String status;
	private int ATK;
	private int DEF;
	private int STA;
	private int SPE;
	private int ID;
	private int AGImax; 
	private char shortName;
	public POOMyPet(String name,char shortName,ArrayList<POOMySkill> newSkill){
		setName(name);
		this.shortName = shortName;
		setStatus("Normal");
		setSkill(newSkill);
		setAGI(0);
		setHP(1000);
		setMP(1000);
		setATK(250);
	}  
	public void setID(int ID){
		this.ID = ID;
	}
	public int getID(){
		return ID;
	}
	public void setSPE(int SPE){
		this.SPE = SPE;
	}
	public int getSPE(){
		return SPE;
	}
	public void setAGImax(int agi){
		AGImax=agi;
	}
	public int getAGImax(){
		return AGImax;
	}
	/** @return SPE if pet is ready to move; 0 otherwise. **/
	public int ready(){
		if(!isAlive()){
			return 0;
		}else if(getAGI() == AGImax){
			return SPE;
		}else{
			return 0;
		}
	}
	/** Recover AGI when timePass
		@return SPE if pet is ready to move; 0 otherwise.
	**/
	public int timePass(){
		if(!isAlive())
			return 0;
		else{
			setAGI(getAGI()+1);
			return ready();
		}
	}
	public void setSkill(ArrayList<POOMySkill> newSkill){
		this.allSkill = newSkill;
	}
	public char getShortName(){
		return shortName;
	}
	protected final boolean setSTA(int STA){
		if(DEF>=0){
			this.STA = STA;
			return true;
		}else{
			return false;
		}
	}
	protected final int getSTA(){
        return STA;
    }
	protected final boolean setDEF(int DEF){
		if(DEF>=0){
			this.DEF = DEF;
			return true;
		}else{
			return false;
		}
	}
	protected final int getDEF(){
		if(getStatus().equals("Iron Defense") || getStatus().equals("Invincible"))
			return 9999;
        return DEF;
    }
	protected final boolean setATK(int ATK){
		if(ATK>=0){
			this.ATK = ATK;
			return true;
		}else{
			return false;
		}
	}
	protected final int getATK(){
		return ATK;
    }
	protected String getStatus(){
		return status;
	}
	protected void setStatus(String newStat){
		status = newStat;
	}
	protected POOPosition setPos(){
		Random ran = new Random();
		return setPos(ran.nextInt(20),ran.nextInt(20));
	}
	protected POOPosition setPos(int x,int y){
		pos = new POOPosition(x,y);
		return pos;
	}
	protected final POOPosition getPos(){
		return pos;
	}
	protected final ArrayList<String> getSkillOrder(){
		return skillOrder;
	}
	/**
       (act defines how the pet would choose a pet
       on the arena (including possibly itself)
       and determine a skill to be used on
       the pet)
	   	These are the reactions after user choose an action.
		1. Find out which skill to perform.
		2. Check pet is able to use the skill.
		3. If so, return the skill; otherwise, return null.
		4. Tell arena to ask user for the target.
		5. Return all things and the arena will call skill.act().
    */
    protected POOAction act(POOArena arena){
		POOAction myAct = new POOAction();
		POOMyArena myA = (POOMyArena) arena;
		String act = cmd2Act(myA.getCmd());
		// stop if command is illegal
		if(act.equals("unknown")){
			System.out.println("Unknown Command");
			return null;
		}else{
			skillOrder.add(act);
			if(skillOrder.size()>5){
				skillOrder.remove(0);
			}
		}
		POOMySkill mySkill = recogSkill();
		myAct.skill = (POOSkill) mySkill;
		// Not using the skill.
		if(mySkill==null){
			System.out.println("You have no skill ready.");
			try{
				Thread.sleep(2000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			redo();
			return null;
		}else if((myAct.dest = myA.getActDest(mySkill))==null){
			System.out.println("Invalid target");
			redo();
			return null;
		}else{
			return myAct;
		}
	}
    /**
       move defines how the pet would want to move in an arena;
       note that the range of moving should be related to AGI
     */
    protected POOCoordinate move(POOArena arena){
		POOMyArena myArena = (POOMyArena) arena;
		// cost AGI 
		int dist_x = Math.abs(pos.x - myArena.getDest()[0]);
		int dist_y = Math.abs(pos.y - myArena.getDest()[1]);
		setAGI(getAGI()-dist_x - dist_y);
		return (POOCoordinate)setPos(myArena.getDest()[0],myArena.getDest()[1]);
	}
	protected void skillDisp(){
		for(int i=3;i<allSkill.size();i++){
			System.out.printf("%c[1;33;40m%s%c[m",27,allSkill.get(i).getName(),27);
			System.out.println(" - " + allSkill.get(i).getDes());
		}
	}
	protected void redo(){
		skillOrder.remove(skillOrder.size()-1);
	}
	protected String cmd2Act(String input){
		if(input.charAt(0)=='A' || input.charAt(0)=='a'){
			return "Atk";
		}else if(input.charAt(0)=='D' || input.charAt(0)=='d'){
			return "Def";
		}else if(input.charAt(0)=='S' || input.charAt(0)=='s'){
			return "SpeM";
		}else if(input.charAt(0)=='R' || input.charAt(0)=='r'){
			return "Rest";
		}else{
			return "unknown";
		}
	}
	protected POOMySkill getASkill(int i){
		return allSkill.get(i);
	}
	protected boolean isAlive(){
		return (getHP()!=0);
	}
	/** Find the skill to be performed by the skill order**/
	protected abstract POOMySkill recogSkill();
}