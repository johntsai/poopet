package ntu.csie.oop13spring;
import java.lang.*;
import java.util.*;

public class POOMyArena extends POOArena{
	//private ArrayList<POOPet> allPets = new ArrayList<POOPet>(0);
	static Scanner scanner = new Scanner(System.in);
	private int[] dest = new int[2];
	private String cmd;
	private char[][] map = new char[20][40];
	private byte[][] obs = new byte[20][20];	// obstacles
	private POOMyPet nextPet;
    
	// initialize
	public POOMyArena(){
		dest[0]=0;
		dest[1]=0;
	}
	public int[] getDest(){
		return dest;
	}
	public void setDest(int a,int b){
		dest[0]=a;
		dest[1]=b;
	}
	public POOMyPet getNextPet(){
		return nextPet;
	}
	public byte[][] getObs(){
		return obs;
	}
	public POOPet getActDest(POOMySkill mySkill){
		String color;
		if(mySkill.getTarget()==0){		// self -> green
			color = "0;30;42";
		}else{							// enemy -> red
			color = "1;37;41";
		}
		// show range
		int range = mySkill.getRange();
		showRange(nextPet.getPos(), range, color, mySkill.isStraight(),mySkill.getType());
	//	interactively get the object of skill, which is a POOPet
		if(mySkill.getTarget()==0){	// self
			return nextPet;
		}else{
			// show all enemy in range
			if(showEnemyInRange(nextPet.getPos(), range,mySkill.isStraight(),mySkill.getType())){
				System.out.printf("\n%c[1;36;40m%s -\n%s\nDetermine your target(input an ID)%c[m\n",27,mySkill.getName(),mySkill.getDes(),27);
				POOPet thePet = getPetByID(scanner.next());
				if(thePet==null){
					System.out.println("Invalid ID!");
					return null;
				}else if(!(((POOMyPet)thePet).isAlive())){
					System.out.println("He's Dead!");
					return null;
				}else if(rangeCheck(((POOMyPet)thePet).getPos() , range , nextPet.getPos(),mySkill.isStraight(),mySkill.getType())){
					return thePet;
				}else{
					return null;
				}
			}else{
				System.out.println("No enemy in range!");
				return null;
			}
		}
	}
	public boolean showEnemyInRange(POOPosition center ,int range, boolean straight,byte type){
		POOPet[] allPets = getAllPets();
		boolean find=false;
		for(int i=0;i<allPets.length;i++){
			POOMyPet myP = (POOMyPet) allPets[i];
			if(rangeCheck(myP.getPos() ,range ,center,straight,type) && myP.isAlive()){	// in range
				System.out.printf("\n%c[1;31;40m%s(%s%d)%c",27,myP.getName(),myP.getShortName(),myP.getID(),27);
				find = true;
			}
		}
		System.out.println(); 
		return find;
	}
	public void show_stat(POOMyPet p){
		showRange(p.getPos(),0,"0;30;43",false,(byte)0);
		System.out.printf("%c[1;36;40m%s's Turn%c[m\n",27,p.getName(),27);
		System.out.printf("%c[1;36;40mHistory of Skill: %c[m",27,27);
		ArrayList<String> mySkillOrder = p.getSkillOrder();
		for(int i=0;i<mySkillOrder.size();i++){
			if(i!=0)
				System.out.print(" > ");
			System.out.print(mySkillOrder.get(i));
		}
		System.out.printf("\n%c[1;36;40mSkill List%c[m\n",27,27);
		p.skillDisp();
	}
	public boolean fight(){
		setAllID();
		POOPet[] allPets = getAllPets();
		nextPet = findNext();
		if(!statusAct(nextPet))	// it cannot move
			return true;
		// one's turn
		boolean redo;
		do{
			show_stat(nextPet);
			int x,y;
			x = nextPet.getPos().x;    
			y = nextPet.getPos().y;
			redo = false;
			System.out.printf("%c[1;36;40mChoose an action:%c[m\n",27,27);
			System.out.println("M: Move (cost: 1 AGI/step)");
			System.out.println("A: Attack - " + nextPet.getASkill(0).getDes() );
			System.out.println("D: Defend - " + nextPet.getASkill(1).getDes());
			System.out.println("S: Special Move - May trigger skills.");
			System.out.println("R: Rest - " + nextPet.getASkill(2).getDes());
			cmd = scanner.next();
			if(cmd.equals("M") || cmd.equals("m")){		// move
				int r=nextPet.getSPE();
				showRange(nextPet.getPos(),r,"1;37;44",true,(byte)0);
				try{
					System.out.printf("\n%c[1;36;40mChoose a direction:%c[m\n",27,27);
					System.out.println("(L)eft\t�� ");
					System.out.println("(R)ight\t��");
					System.out.println("(U)p\t��");
					System.out.println("(D)own\t��");
					cmd = scanner.next();
					cmd = cmd.toLowerCase().substring(0);
					if(!cmd.equals("l") && !cmd.equals("r") && !cmd.equals("u") && !cmd.equals("d"))
						throw new InputMismatchException("");
					System.out.printf("\n%c[1;36;40mHow many steps do you want to move? (1-%d)%c[m\n",27,nextPet.getSPE(),27);
					int tmp = scanner.nextInt();
					if(cmd.equals("l")){
						x = nextPet.getPos().x-tmp;
					}else if(cmd.equals("r")){
						x = nextPet.getPos().x+tmp;
					}else if(cmd.equals("u")){
						y = nextPet.getPos().y-tmp;
					}else if(cmd.equals("d")){
						y = nextPet.getPos().y+tmp;
					}
				}catch(InputMismatchException e){
					System.out.println("Input Error!");
					try{
						Thread.sleep(1000);
					}catch(InterruptedException e2){
						e2.printStackTrace();
					}
					redo = true;
					continue;
				}
				if(rangeCheck(x,y,r,nextPet.getPos(),true,(byte)0) && getPetByPos(x,y)==null){
					dest[0]=x;
					dest[1]=y;
					nextPet.move(this);
					// update the skill order
					nextPet.getSkillOrder().add("Mov");
					if(nextPet.getSkillOrder().size()>5){
						nextPet.getSkillOrder().remove(0);
					}
				}else{
					System.out.println("You cannot go there!");
					try{
						Thread.sleep(1500);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
					redo = true;
				}
			}else{										// other command
				POOAction myAct = nextPet.act(this);
				if(myAct == null){
					try{
						Thread.sleep(1500);
					}catch(InterruptedException e){
						e.printStackTrace();
					}
					redo = true;
				}else{
					POOMySkill theSkill = (POOMySkill)myAct.skill;
					if(theSkill.check(nextPet)){		// check if pet have enough MP/HP...
						theSkill.setVar(this);
						theSkill.act(myAct.dest);
						theSkill.cost(nextPet);
						try{
							Thread.sleep(500);
						}catch(InterruptedException e){
							e.printStackTrace();
						}
					}else{
						System.out.println("You do not have enough MP/HP/AGI!");
						redo = true;
					}
				}
			}
		}while(redo);
		// if GameOver, return false; end
		allPets = getAllPets();
		POOMyPet winPet = (POOMyPet)allPets[0];
		int count=0;
		for(int i=0;i<allPets.length;i++){
			POOMyPet myP = (POOMyPet) allPets[i];
			if(myP.isAlive()){
				winPet = myP;
				count++;
			}
		}
		if(count<2){
			System.out.println("\nGame Over!");
			System.out.printf("%s(%s%d) Win!\n",winPet.getName(),winPet.getShortName(),winPet.getID());
			return false;
		}else{
			return true;
		}
	}
	public void show(){
		show(null);
	}
    public void show(POOPosition center){
		 showRange(center,0,"",false);
	}
	public void showRange(POOPosition center, int r, String color,boolean straight){
		showRange(center,r,color,straight,(byte)0);
	}
	public void showRange(POOPosition center, int r, String color,boolean straight,byte type){
		setMap();
		POOPet[] myAllPets = getAllPets();
		POOMyPet showPet =(POOMyPet)myAllPets[0];
		int showInt = 0;
		System.out.print((char)27 + "[2J");
		System.out.print((char)27 + "[H");
		// x-axis 
		System.out.print("   �z�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�{");
		System.out.printf("\t\t%s(%s%d)\n",showPet.getName(),showPet.getShortName(),showPet.getID());
		for(int i=0;i<20;i++){
			System.out.printf("%2d �x",i);
			// if center is not null
			for(int j=0;j<20;j++){ 
				if(center!=null){
					if(rangeCheck(j,i,r,center,straight,type)){
						System.out.printf("%c[%sm",27,color);
						if(map[i][j*2]==' '){
							System.out.print("�I");
						}else{
							System.out.printf("%c%c",map[i][j*2],map[i][j*2+1]);
						}
						System.out.printf("%c[m",27);
					}else{
						if(map[i][j*2]==' '){
							System.out.print("�E");
						}else{
							System.out.printf("%c%c",map[i][j*2],map[i][j*2+1]);
						}
					}
				}else{
					if(map[i][j*2]==' '){
						System.out.print("�E");
					}else{
						System.out.printf("%c%c",map[i][j*2],map[i][j*2+1]);
					}
				}				
			}
			System.out.print("�x "+i+"\t");
			// show stat on the right-hand-side
			if(showPet==null){
				System.out.println();
			}else{
				if(i%6==5){
					System.out.printf("%s(%s%d)\n",showPet.getName(),showPet.getShortName(),showPet.getID());
				}else if(i%6==0){
					System.out.printf("HP: %d/1000\n",showPet.getHP());
				}else if(i%6==1){
					System.out.printf("MP: %d/1000\n",showPet.getMP());
				}else if(i%6==2){
					System.out.printf("AGI: %d/%d (-%d)\n",showPet.getAGI(),showPet.getAGImax(),showPet.getAGImax()-showPet.getAGI());
				}else if(i%6==3){
					System.out.println(showPet.getStatus());
				}else if(i%6==4){
					if(showInt+1>=myAllPets.length){
						showPet=null;
					}else{
						showInt++;
						showPet = (POOMyPet) myAllPets[showInt];
					}
					System.out.println();
				}
			}
		}
		System.out.print("   �|�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�w�}");
		if(showPet!=null)
			System.out.printf("\t\tAGI: %d/%d\n",showPet.getAGI(),showPet.getAGImax());
		else
			System.out.println();
		System.out.print("     ");
		for(int i=0;i<20;i++){
			if(i%2==1)
				System.out.printf("%2d",i);
			else
				System.out.print("  ");
		}
		if(showPet!=null)
			System.out.print("\t\t"+showPet.getStatus());
		System.out.println();
	}
    public POOCoordinate getPosition(POOPet p){
		POOMyPet myP = (POOMyPet)p;
		return myP.getPos();
	}
	public String getCmd(){
		return cmd;
	}
	public POOPet getPetByID(String input){
		try{
			int id = Integer.parseInt(input); 
			return getPetByID(id);
		}
		catch(NumberFormatException e){	
			return null;
		}
	}
	public POOPet getPetByID(int id){
		POOPet[] myallPets = getAllPets();
		if(id>0 && id<=myallPets.length)
			return myallPets[id-1];
		else
			return null;
	}
	public POOPet getPetByPos(int x,int y){
		POOPet[] myallPets = getAllPets();
		for(int i=0;i<myallPets.length;i++){
			POOMyPet myP = (POOMyPet) myallPets[i];
			if(myP.getPos().equals(x,y)){	// Found
				return myallPets[i];
			}
		}
		// not found
		return null;
	}
	/** type 0 - move
		type 1 - attack
		type 2 - piercing attack
	**/
	public boolean rangeCheck(int x,int y,int r,POOPosition center,boolean straight){
		return rangeCheck(x,y,r,center,straight,(byte)0);
	}
	public boolean rangeCheck(int x,int y,int r,POOPosition center,boolean straight,byte type){
		// prevention of going out of bound
		if(!borderCheck(x,y))
			return false;
		//	straight check for move
		if(straight && center.x!=x&& center.y!=y && type==0)
			return false;
		// attack may work on 45 degree
		else if(straight && center.x!=x && center.y!=y && Math.abs(center.y-y)!=Math.abs(center.x-x))
			return false;
		// obs check
		if(straight && type!=(byte)2){
			if(type==0 && obs[y][x]!=0)	// you cannot move onto the other
				return false;
			if(center.y==y && center.x!=x){
				for(int i=center.x ; i!=x ; i+=(x-center.x)/Math.abs(x-center.x)){
					if(i==center.x)
						continue;
					else if(obs[y][i]!=0){
						return false;
					}
				}
			}else if(center.y!=y && center.x==x){
				for(int i=center.y ; i!=y ; i+=(y-center.y)/Math.abs(y-center.y)){
					if(i==center.y)
						continue;
					else if(obs[i][x]!=0){
						return false;
					}
				}
			}else{ 
				int i= center.x;
				int j= center.y;
				for(; i!=x && j!=y ; j+=(y-center.y)/Math.abs(y-center.y), i+=(x-center.x)/Math.abs(x-center.x)){
					if(i==center.x)
						continue;
					else if(obs[j][i]!=0)
						return false;
				}
			}
		} 
		int delt_x = Math.abs(x - center.x);
		int delt_y = Math.abs(y - center.y);
		return (delt_x+delt_y <= r);
	}
	public boolean rangeCheck(POOPosition target,int r,POOPosition center,boolean straight){
		return rangeCheck(target,r,center,straight,(byte)0);
	}
	public boolean rangeCheck(POOPosition target,int r,POOPosition center,boolean straight,byte type){
		// don't attack yourself!
		if(target == center)
			return false;
		else
			return rangeCheck(target.x,target.y,r,center,straight,type);
	}
	public boolean borderCheck(int x,int y){
		return (x>=0 && x<20) && (y>=0 && y<20);
	}
	public void setAllID(){
		POOPet[] myallPets = getAllPets();
		for(int i=0;i<myallPets.length;i++){
			POOMyPet myP = (POOMyPet) myallPets[i];
			myP.setID(i+1);
		}
	}
	public void setMap(){		
		POOPet[] myallPets = getAllPets();
		int i,j;
		for(i=0;i<20;i++){
			for(j=0;j<20;j++){
					map[i][2*j]=' ';
					map[i][2*j+1]=' ';
					obs[i][j]=0;
			}
		}
		for(i=0;i<myallPets.length;i++){
			POOMyPet myP = (POOMyPet) myallPets[i];
			if(myP.isAlive()){
				POOPosition pos = myP.getPos();
				int x = pos.x;
				int y = pos.y;
				map[y][2*x] = myP.getShortName();
				map[y][2*x+1] = (char)('0'+myP.getID());
				obs[y][x] = (byte)myP.getID();
			}
		}
	}
	/** Find the next pet to move. Also makes time pass.
		@return The ready pet with higher AGI.
	**/
	public POOMyPet findNext(){
		POOPet[] allPets = getAllPets();
		int[] allSPE = new int[allPets.length];
		boolean find = false;
		// Is any one ready?
		for(int i=0;i<allPets.length;i++){
			POOMyPet myP = (POOMyPet) allPets[i];
			if((allSPE[i] = myP.ready())!=0)
				find = true;
		}
		while(!find){		// then let time pass
			for(int i=0;i<allPets.length;i++){
				POOMyPet myP = (POOMyPet) allPets[i];
				if((allSPE[i] = myP.timePass())!=0)
					find = true;
			}
		}
		// find the fastest one to move
		int max = 0;
		int index=-1;
		for(int i=0;i<allPets.length;i++){
			if(allSPE[i]>max){
				max = allSPE[i];
				index = i;
			}
		}
		return (POOMyPet) allPets[index];
	}
	public boolean statusAct(POOMyPet pet){
		if(pet.getStatus().equals("Defend")){
			pet.setDEF(pet.getDEF()/2);
			pet.setStatus("Normal");
			return true;
		}else if(pet.getStatus().equals("Iron Defense")){
			pet.setStatus("Normal");
			pet.setAGI(pet.getAGI()-8);
			return false;
		}else{
			pet.setStatus("Normal");
			return true;
		}
	} /*
	// For testing
	public static void main(String argv[]){
		POOMyArena myA = new POOMyArena();
		POOArcher[] pets = new POOArcher[4];
		for(int i=0;i<4;i++){
			pets[i] = new POOArcher();
			myA.addPet(pets[i]);
		}
		myA.showRange(pets[0].getPos(),10,"1;37;41",true,(byte) 1);
		System.out.println(pets[0].getPos().x + " " + pets[0].getPos().y);
	}
	// copy from fight
	 public static void main(String argv[]){
        POOArena arena = null;
        POOPet pet = null;

		for(int i=0;i<argv.length;i++)
			System.out.println(argv[i]);
        if (argv.length < 3){
            System.out.println("Usage: java arena_class arena_class pet1_class pet2_class ...");
            return;
        }

        String arena_class = argv[0];
        try{
            arena = (POOArena)(Class.forName(arena_class).newInstance());
        }
        catch(Exception e){
            System.out.println(e);
            return;
        }

        for(int i=1;i<argv.length;i++){
            String pet_class = argv[i];

            try{
                pet = (POOPet)(Class.forName(pet_class).newInstance());
                arena.addPet(pet);
            }
            catch(Exception e){
                System.out.println(e);
                return;
            }
        }
        while(arena.fight()){
            arena.show();
        }
    }*/
}