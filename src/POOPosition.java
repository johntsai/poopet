package ntu.csie.oop13spring;

public class POOPosition extends POOCoordinate{
	// construction
	public POOPosition(int _x,int _y){
		this.x = _x;
		this.y = _y;
	}
	public boolean equals(POOCoordinate other){
		return (this.x == other.x)&&(this.y==other.y);
	}
	public boolean equals(int x,int y){
		return (this.x == x)&&(this.y==y);
	}
}